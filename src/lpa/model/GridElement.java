/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Represents an element of the puzzle grid.
 */
public abstract class GridElement {
    
    /**
     * Row number of the element.
     */
    protected int I;
    
    /**
     * Column number of the element.
     */
    protected int J;
    
    /**
     * Element group of the element.
     */
    protected ElementGroup surroundings = new ElementGroup();
    
    /**
     * Is the element violated or not.
     */
    protected boolean violated = false;
    
    /**
     * Constructor.
     * @param I row number of the element.
     * @param J column number of the element
     */
    public GridElement(int I, int J) {
        if (I < 0 || J < 0) {
            throw new IllegalArgumentException("In lpa.model.GridElement.GridElement: X and Y are both have to be more than zero");
        }
        
        this.I = I;
        this.J = J;
    }
    
    /**
     * Returns row number of the element.
     * @return row number of the element
     */
    public int getI() {
        return I;
    }
    
    /**
     * Returns column number of the element.
     * @return column number of the element
     */
    public int getJ() {
        return J;
    }
    
    /**
     * Initializes element group of the element.
     * @param grid grid where the element is located
     * @param width width of the grid which contains the element
     * @param height height of the grid which contains the element
     */
    public abstract void initSurroundings(List<List<GridElement>> grid, int width, int height);
    
    /**
     * Returns element group of the element.
     * @return element group of the element
     */
    public ElementGroup getSurroundings() {
        return surroundings;
    }
    
    /**
     * Sets the violation state of the element.
     * @param violated new state
     */
    public void setViolated(boolean violated) {
        this.violated = violated;
    }
    
    /**
     * Returns violation state of the element.
     * @return violation state of the element
     */
    public boolean getViolated() {
        return violated;
    }
    
}
