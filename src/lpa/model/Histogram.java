/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Represents histogram used to determine number of edges with specified state in the group.
 */
public class Histogram extends EnumMap<EdgeState, Integer> {
    
    /**
     * Constructor.
     */
    public Histogram() {
        super(EdgeState.class);
    }
    
    /**
     * Updates information about given element group.
     * @param group element group with source information
     */
    public void update(ElementGroup group) {
        if (group == null) {
            throw new IllegalArgumentException("In lpa.model.Histogram.init: group cannot be null");
        }
        
        for (EdgeState state : EdgeState.values()) {
            put(state, 0);
        }
        
        for (GridElement element : group) {
            if (!(element instanceof Edge)) {
                continue;
            }
            EdgeState state = ((Edge)element).getState();
            put(state, get(state) + 1);
        }
    }
    
    /**
     * Returns number of edges in the source element group.
     * @return number of edges in the source element group
     */
    public int getCount() {
        int count = 0;
        for (EdgeState state : EdgeState.values()) {
            count += get(state);
        }
        return count;
    }
    
    /**
     * Returns number of absent edges in the group.
     * @return number of absent edges
     */
    public int getAbsent() {
        return get(EdgeState.ABSENT);
    }
    
    /**
     * Returns number of present edges in the group.
     * @return number of present edges
     */
    public int getPresent() {
        return get(EdgeState.PRESENT);
    }
    
    /**
     * Returns number of undetermined edges in the group.
     * @return number of undetermined edges
     */
    public int getUndetermined() {
        return get(EdgeState.UNDETERMINED);
    }
    
}
