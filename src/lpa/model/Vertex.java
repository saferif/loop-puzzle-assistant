/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Represents a vertex in puzzle grid.
 */
public class Vertex extends GridElement {
    
    /**
     * Constructor.
     * @param I row number of the vertex
     * @param J column number of the vertex
     */
    public Vertex(int I, int J) {
        super(I, J);
    }
    
    /**
     * Initializes element group of the vertex so that it contains all edges connected to it.
     * @param grid grid where the vertex is located
     * @param width width of the grid which contains the vertex
     * @param height height of the grid which contains the vertex
     */
    @Override
    public void initSurroundings(List<List<GridElement>> grid, int width, int height) {
        if (grid == null) {
            throw new IllegalArgumentException("In lpa.model.Vertex.initSurroundings: Grid cannot be null");
        }
        //Top edge
        if (I != 0) {
            surroundings.addElement(grid.get(I - 1).get(J));
        }
        //Left edge
        if (J != 0) {
            surroundings.addElement(grid.get(I).get(J - 1));
        }
        //Bottom edge
        if (I != height - 1) {
            surroundings.addElement(grid.get(I + 1).get(J));
        }
        //Right edge
        if (J != width - 1) {
            surroundings.addElement(grid.get(I).get(J + 1));
        }
    }
    
}
