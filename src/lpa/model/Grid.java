/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;
import lpa.solvers.strategies.CornerOneStrategy;
import lpa.solvers.strategies.CornerThreeStrategy;
import lpa.solvers.strategies.DeadEndPreventionStrategy;
import lpa.solvers.strategies.FullCellStrategy;
import lpa.solvers.strategies.FullVertexStrategy;
import lpa.solvers.strategies.LackingCellStrategy;
import lpa.solvers.strategies.Strategy;
import lpa.solvers.violations.Violations;

/**
 * Represents puzzle grid.
 */
public class Grid implements Iterable<GridElement> {

    /**
     * List of all the elements in the grid.
     */
    private List<List<GridElement>> grid;
    
    /**
     * Width of the grid.
     */
    private int width = 0;
    
    /**
     * Height of the grid.
     */
    private int height = 0;
    
    /**
     * Object used to determine violations to check.
     */
    private Violations violations = new Violations();
    
    /**
     * List of strategy object to apply to the grid.
     */
    private List<Strategy> strategies;
    
    /**
     * Initializes strategy objects.
     */
    private void initStrategies() {
        strategies = new ArrayList<>();
        strategies.add(new CornerOneStrategy());
        strategies.add(new CornerThreeStrategy());
        strategies.add(new DeadEndPreventionStrategy());
        strategies.add(new FullCellStrategy());
        strategies.add(new FullVertexStrategy());
        strategies.add(new LackingCellStrategy());
    }

    /**
     * Constructor.
     * @param scanner scanner from which to retrieve the information about the grid
     */
    public Grid(Scanner scanner) {
        if (scanner == null) {
            throw new NullPointerException("In lpa.model.Grid.Grid: Scanner cannot be null");
        }

        grid = new ArrayList<>();
        while (scanner.hasNext()) {
            grid.add(new ArrayList<GridElement>());

            String line = scanner.nextLine();
            if (height == 0) {
                width = line.length();
            }
            //All lines must be equal length
            if (line.length() != width) {
                throw new IllegalArgumentException("In lpa.model.Grid.Grid: lines must be the same length");
            }
            for (int i = 0; i < width; i++) {
                switch (line.charAt(i)) {
                    //Vertex
                    case '+': {
                        if ((height & 1) + (i & 1) != 0) {
                            throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: + is illegal in place {%d, %d}", height, i));
                        }
                        grid.get(height).add(new Vertex(height, i));
                        break;
                    }
                    //Horizontal present edge
                    case '-': {
                        if (((height & 1) == 1) || ((i & 1) == 0)) {
                            throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: - is illegal in place {%d, %d}", height, i));
                        }
                        grid.get(height).add(new Edge(height, i, EdgeState.PRESENT));
                        break;
                    }
                    //Vertical present edge
                    case '|': {
                        if (((height & 1) == 0) || ((i & 1) == 1)) {
                            throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: | is illegal in place {%d, %d}", height, i));
                        }
                        grid.get(height).add(new Edge(height, i, EdgeState.PRESENT));
                        break;
                    }
                    //Undetermined edge
                    case '.': {
                        if ((height & 1) + (i & 1) != 1) {
                            throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: . is illegal in place {%d, %d}", height, i));
                        }
                        grid.get(height).add(new Edge(height, i, EdgeState.UNDETERMINED));
                        break;
                    }
                    //Cell with specified numer in it
                    case '0':
                    case '1':
                    case '2':
                    case '3': {
                        if ((height & 1) + (i & 1) != 2) {
                            throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: %s is illegal in place {%d, %d}", line.charAt(i), height, i));
                        }
                        grid.get(height).add(new Cell(height, i, Integer.parseInt(line.charAt(i) + "")));
                        break;
                    }
                    case ' ': {
                        //Empty cell
                        if ((height & 1) + (i & 1) == 2) {
                            grid.get(height).add(new Cell(height, i, null));
                            break;
                        }
                        
                        //Absent edge
                        if ((height & 1) + (i & 1) == 1) {
                            grid.get(height).add(new Edge(height, i, EdgeState.ABSENT));
                            break;
                        }
                        throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: space is illegal in place {%d, %d}", height, i));
                    }
                    //Other symbols are not allowed
                    default: {
                        throw new IllegalArgumentException(String.format("In lpa.model.Grid.Grid: %s is illegal in place {%d, %d}", line.charAt(i), height, i));
                    }
                }
            }
            height++;
        }
        //Correct grid has to have odd width and odd height
        if (((width & 1) == 0) || ((height & 1) == 0)) {
            throw new IllegalArgumentException("In lpa.model.Grid.Grid: ilvalid grid dimensions.");
        }
        
        //Initialize element groups
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                grid.get(i).get(j).initSurroundings(grid, width, height);
            }
        }
        
        //Initialize strategies
        initStrategies();
    }

    /**
     * Returns width of the grid.
     * @return width of the grid
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height of the grid.
     * @return height of the grid 
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * Returns an object containing information about violations to be checked.
     * @return violations object
     */
    public Violations getViolations() {
        return violations;
    }

    /**
     * Returns an element on specified position.
     * @param I row number
     * @param J column number
     * @return an element
     */
    public GridElement getElement(int I, int J) {
        if (I < 0 || J < 0 || I >= height || J >= width) {
            throw new IllegalArgumentException("In lpa.model.Grid.getElement: X or Y out of range");
        }

        return grid.get(I).get(J);
    }

    /**
     * Returns iterator.
     * @return iterator
     */
    @Override
    public Iterator<GridElement> iterator() {
        return new Iterator<GridElement>() {
            private int columnIndex = 0, rowIndex = 0;

            @Override
            public boolean hasNext() {
                return rowIndex < height && columnIndex < width;
            }

            @Override
            public GridElement next() {
                if (!hasNext()) {
                    throw new NoSuchElementException("GridIterator.next()");
                }

                final GridElement result = grid.get(rowIndex).get(columnIndex);
                ++columnIndex;
                if (columnIndex == width) {
                    columnIndex = 0;
                    ++rowIndex;
                }
                return result;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    /**
     * Returns string representing current state of the grid.
     * @return string representing current state of the grid
     */
    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < height; i++) {
            String line = "";
            for (int j = 0; j < width; j++) {
                GridElement element = grid.get(i).get(j);
                //Cell
                if (element instanceof Cell) {
                    Cell cell = (Cell) element;
                    if (cell.getDigit() == null) {
                        //Cell is empty
                        line += " ";
                    } else {
                        //Cell contains a digit
                        line += cell.getDigit();
                    }
                }
                //Edge
                if (element instanceof Edge) {
                    Edge edge = (Edge) element;
                    switch (edge.getState()) {
                        case ABSENT: {
                            line += " ";
                            break;
                        }
                        case UNDETERMINED: {
                            line += ".";
                            break;
                        }
                        case PRESENT: {
                            if ((edge.getI() & 1) == 0) {
                                line += "-";
                            } else {
                                line += "|";
                            }
                            break;
                        }
                    }
                }
                //Vertex
                if (element instanceof Vertex) {
                    line += '+';
                }
            }
            str += line + "\n";
        }
        return str;
    }
    
    /**
     * Updates information about violated elements.
     */
    public void updateViolatedElements() {
        for (GridElement element : this) {
            Histogram histogram = element.getSurroundings().getHistogram();
            element.setViolated(false);
            if (element instanceof Cell) {
                Cell cell = (Cell)element;
                //Cell has too many present edges around it
                if (violations.getYesEdgesState()) {
                    if (cell.getDigit() != null) {
                        if (histogram.getPresent() > cell.getDigit()) {
                            cell.setViolated(true);
                        }
                    }
                }
                //Cell has too many absent edges around it
                if (violations.getNoEdgesState()) {
                    if (cell.getDigit() != null) {
                        if (histogram.getAbsent() > 4 - cell.getDigit()) {
                            cell.setViolated(true);
                        }
                    }
                }
            }
            if (element instanceof Vertex) {
                Vertex vertex = (Vertex)element;
                //Branching path will cause to impossibitity to create a loop
                if (violations.getBranchingPathState()) {
                    if (histogram.getPresent() > 2) {
                        vertex.setViolated(true);
                    }
                }
                //Dead end, impossible to continue the loop
                if (violations.getDeadEndState()) {
                    if (histogram.getPresent() == 1 && histogram.getAbsent() == histogram.getCount() - 1) {
                        vertex.setViolated(true);
                    }
                }
            }
        }
    }

    /**
     * Checks whether grid has violated elements or not.
     * @return true if grid has some violated elements, and false otherwise
     */
    private boolean isVioated() {
        for (GridElement element : this) {
            if (element instanceof Edge) {
                element.setViolated(false);
            }
            if (element.getViolated()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Clones the grid.
     * @return new grid
     */
    @Override
    public Grid clone() {
        return new Grid(new Scanner(toString()));
    }

    /**
     * Created empty list of checked elements during solution check.
     * @return empty list of checked elements
     */
    private List<List<Boolean>> initCheckedList() {
        List<List<Boolean>> list = new ArrayList<>(height);
        for (int i = 0; i < height; i++) {
            List<Boolean> l = new ArrayList<>(width);
            for (int j = 0; j < width; j++) {
                l.add(false);
            }
            list.add(l);
        }
        return list;
    }

    /**
     * Finds first unchecked undetermined edge.
     * @param checked list of checked elements
     * @return found edge if there is such, null otherwise
     */
    private Edge findFirstEdge(List<List<Boolean>> checked) {
        for (GridElement element : this) {
            if (element instanceof Edge) {
                Edge edge = (Edge) element;
                if (edge.getState() == EdgeState.PRESENT && (!checked.get(edge.getI()).get(edge.getJ()))) {
                    return edge;
                }
            }
        }
        return null;
    }

    /**
     * Finds next unchecked undetermined edge.
     * @param checked list of checked elements
     * @param previous previously found edge
     * @param current current edge
     * @param start first found edge
     * @return found edge if there is such, null otherwise
     */
    private Edge findNextEdge(List<List<Boolean>> checked, Edge previous, Edge current, Edge start) {
        for (GridElement geVertex : current.getSurroundings()) {
            if (!(geVertex instanceof Vertex)) {
                //If element is not vertex, continue search
                continue;
            }
            Vertex vertex = (Vertex) geVertex;
            for (GridElement geEdge : vertex.getSurroundings()) {
                if (!(geEdge instanceof Edge)) {
                    throw new IllegalArgumentException("In lpa.model.grid.findNextEdge: surroundings of vertex must be edges.");
                }
                Edge edge = (Edge) geEdge;
                if (edge != previous && edge != current && edge.getState() == EdgeState.PRESENT) {
                    if (!checked.get(edge.getI()).get(edge.getJ()) || edge == start) {
                        return edge;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Tries to find a loop in the grid.
     * @param checked list of checked elements
     * @return whether the loop is found or not
     */
    private boolean findMainLoop(List<List<Boolean>> checked) {
        //Finding the first edge in the loop
        Edge startEdge = findFirstEdge(checked);
        if (startEdge == null) {
            return true;
        }
        //First edge is checked
        checked.get(startEdge.getI()).set(startEdge.getJ(), true);
        //Finding the next edge in the loop
        Edge currentEdge = findNextEdge(checked, null, startEdge, startEdge);
        Edge previousEdge = startEdge;
        while (currentEdge != null && currentEdge != startEdge) {
            //Trying to continue the loop until we are back to the begining
            checked.get(currentEdge.getI()).set(currentEdge.getJ(), true);
            Edge nextEdge = findNextEdge(checked, previousEdge, currentEdge, startEdge);
            previousEdge = currentEdge;
            currentEdge = nextEdge;
        }
        //If the loop is cut, this is not the loop
        return currentEdge != null;
    }

    /**
     * Searches for unchecked edges outside the loop.
     * @param checked list of checked elements
     * @return whether extra edges are found or not
     */
    private boolean findExtraEdges(List<List<Boolean>> checked) {
        boolean found = false;
        for (GridElement element : this) {
            if (element instanceof Edge && ((Edge) element).getState() == EdgeState.PRESENT) {
                if (!checked.get(element.getI()).get(element.getJ())) {
                    found = true;
                    element.setViolated(true);
                }
            }
        }
        return found;
    }

    /**
     * Checks whether grid is correctly solved or not.
     * @return whether grid is correctly solved or not
     */
    public boolean isSolved() {
        boolean violated = isVioated();
        List<List<Boolean>> checked = initCheckedList();
        boolean loopFound = findMainLoop(checked);
        boolean extraEdgesFound = false;
        if (loopFound) {
            extraEdgesFound = findExtraEdges(checked);
        }
        return (!violated) && (!extraEdgesFound) && loopFound;
    }

    /**
     * Applies strategies to the grid.
     * @return edit made by strategies
     */
    public PuzzleCompoundUndoableEdit applyStrategies() {
        boolean applied = true;
        PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
        while (applied) {
            applied = false;
            for (GridElement element : this) {
                for (Strategy strategy : strategies) {
                    PuzzleCompoundUndoableEdit e = strategy.apply(element, getWidth(), getHeight());
                    if (e != null && e.isSignificant()) {
                        edit.add(e);
                        e.applyEdit();
                        applied = true;
                    }
                }
            }
        }
        return edit.isSignificant() ? edit : null;
    }
}
