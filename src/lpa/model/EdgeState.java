/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

/**
 * State of an edge
 */
public enum EdgeState {
    
    UNDETERMINED, 
    PRESENT, 
    ABSENT
    
}
