/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Group of elements contained in the grid.
 */
public class ElementGroup implements Iterable<GridElement> {
    
    /**
     * List of elements in the group.
     */
    private List<GridElement> elements;
    
    /**
     * Histogram used to determine number of edges with specified state in the group.
     */
    private Histogram histogram = new Histogram();
    
    /*
     * Contructor.
     */
    public ElementGroup() {
        elements = new ArrayList<>();
    }
    
    /**
     * Adds an element into the group.
     * @param element element to add
     */
    public void addElement(GridElement element) {
        elements.add(element);
        updateHistogram();
    }
    
    /**
     * Returns an element with specified index from the group.
     * @param index index of an element to return
     * @return an element
     */
    public GridElement getElement(int index) {
        if (index < 0 || index > elements.size() - 1) {
            throw new NoSuchElementException("In lpa.model.ElementGroup.getElement: illegal index");
        }
        return elements.get(index);
    }

    /**
     * Returns iterator of the group.
     * @return iterator
     */
    @Override
    public Iterator<GridElement> iterator() {
        return elements.iterator();
    }
    
    /**
     * Forces the histogram of the group to update.
     */
    public void updateHistogram() {
        histogram.update(this);
    }
    
    /**
     * Returns the histogram of the group.
     * @return histogram
     */
    public Histogram getHistogram() {
        return histogram;
    }
    
}
