/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Represents a cell in puzzle grid.
 */
public class Cell extends GridElement {
    
    /**
     * Digit in the cell.
     */
    private Integer digit;
    
    /**
     * Constructor.
     * @param I row number of the cell
     * @param J column number of the cell
     * @param digit digit in the cell
     */
    public Cell(int I, int J, Integer digit) {
        super(I, J);
        
        if (digit != null && (digit < 0 || digit > 3)) {
            throw new IllegalArgumentException("In lpa.model.Cell.Cell: Digit can only be in range [0; 3] or null");
        }
        
        this.digit = digit;
    }
    
    /**
     * Initializes element group of the cell so that it contains all edges around itself.
     * @param grid grid where the cell is located
     * @param width width of the grid which contains the cell
     * @param height height of the grid which contains the cell
     */
    @Override
    public void initSurroundings(List<List<GridElement>> grid, int width, int height) {
        if (grid == null) {
            throw new IllegalArgumentException("In lpa.model.Cell.initSurroundings: Grid cannot be null");
        }
        //Top edge
        if (I != 0) {
            surroundings.addElement(grid.get(I - 1).get(J));
        }
        //Left edge
        if (J != 0) {
            surroundings.addElement(grid.get(I).get(J - 1));
        }
        //Bottom edge
        if (I != height - 1) {
            surroundings.addElement(grid.get(I + 1).get(J));
        }
        //Right edge
        if (J != width - 1) {
            surroundings.addElement(grid.get(I).get(J + 1));
        }
        
    }
    
    /**
     * Sets digit property of the cell.
     * @param digit new digit in the cell
     */
    public void setDigit(Integer digit) {
        if (digit != null) {
            if (digit < 0 || digit > 3) {
                throw new IllegalArgumentException("In lpa.model.Cell.setDigit: Digit can only be in range [0; 3] or null");
            }
        }
        this.digit = digit;
    }
    
    /**
     * Returns current digit in the cell.
     * @return digit in the cell
     */
    public Integer getDigit() {
        return digit;
    }
    
}
