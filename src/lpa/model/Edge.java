/**
 * @author Latypov Insaf, 271PI
 */

package lpa.model;

import java.util.*;

/**
 * Represents an edge in puzzle grid.
 */
public class Edge extends GridElement {
    
    /**
     * State of the edge.
     */
    private EdgeState state;
    
    /**
     * Constructor.
     * @param I row number of the edge
     * @param J column number of the edge
     * @param state state of the edge
     */
    public Edge(int I, int J, EdgeState state) {
        super(I, J);
        
        if (state == null) {
            throw new IllegalArgumentException("In lpa.model.Edge.Edge: State cannot be null");
        }
        
        this.state = state;
    }
    
    /**
     * Sets the state of the edge.
     * @param value new state of the edge
     */
    public void setState(EdgeState value) {
        state = value;
    }
    
    /**
     * Returns current state of the edge.
     * @return current state of the edge
     */
    public EdgeState getState() {
        return state;
    }

    /**
     * Initializes element group of the edge so that it contains two vertices and two cells around itself.
     * @param grid grid where the edge is located
     * @param width width of the grid which contains the edge
     * @param height height of the grid which contains the edge
     */
    @Override
    public void initSurroundings(List<List<GridElement>> grid, int width, int height) {
        if (grid == null) {
            throw new IllegalArgumentException("In lpa.model.Edge.initSurroundings: Grid cannot be null");
        }
        //Left element
        if (J > 0) {
            surroundings.addElement(grid.get(I).get(J - 1));
        }
        //Right element
        if (J < grid.get(I).size() - 1) {
            surroundings.addElement(grid.get(I).get(J + 1));
        }
        //Top element
        if (I > 0) {
            surroundings.addElement(grid.get(I - 1).get(J));
        }
        //Bottom element
        if (I < grid.size() - 1) {
            surroundings.addElement(grid.get(I + 1).get(J));
        }
    }
    
}
