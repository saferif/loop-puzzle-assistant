/**
* The Loop Puzzle Assistant model.
*
* Contains classes to represent all the possible grid elements.
*/
package lpa.model;