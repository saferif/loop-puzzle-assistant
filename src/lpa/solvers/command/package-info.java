/**
 * The Loop Puzzle Assistant commands.
 * 
 * Contains classes related to undo, redo feature.
 * <ul>
 * <li>PuzzleAbstractUndoableEdit - abstract edit.</li>
 * <li>PuzzleCompoundUndoableEdit - compound edit (contains several simple edits).</li>
 * <li>PuzzleUndoableEdit - simple edit.</li>
 * </ul>
 */

package lpa.solvers.command;