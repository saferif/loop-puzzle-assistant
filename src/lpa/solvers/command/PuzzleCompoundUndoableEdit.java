/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.command;

import java.util.ArrayList;
import java.util.List;
import javax.swing.undo.UndoableEdit;

/**
 * Represents compound edit.
 */
public class PuzzleCompoundUndoableEdit extends PuzzleAbstractUndoableEdit {
    
    /**
     * List of edits.
     */
    List<PuzzleAbstractUndoableEdit> edits = new ArrayList<>();
    
    /**
     * Adds an edit to the list.
     * @param edit edit to add
     */
    public void add(PuzzleAbstractUndoableEdit edit) {
        if (edit.isSignificant()) {
            edits.add(edit);
        }
    }
    
    /**
     * Constructor.
     */
    public PuzzleCompoundUndoableEdit() {
        super();
    }
    
    /**
     * Undo.
     */
    @Override
    public void undo() {
        super.undo();
        for (int i = edits.size() - 1; i >= 0; i --) {
            edits.get(i).undo();
        }
    }
    
    /**
     * Redo.
     */
    @Override
    public void redo() {
        super.redo();
        for (UndoableEdit edit : edits) {
            edit.redo();
        }
    }
    
    /**
     * Determines if the edit is significant or not.
     * @return whether the edit is significant or not
     */
    @Override
    public boolean isSignificant() {
        boolean result = false;
        for (UndoableEdit edit : edits) {
            result |= edit.isSignificant();
        }
        return result;
    }

    /**
     * Applies the edit.
     */
    @Override
    public void applyEdit() {
        for (PuzzleAbstractUndoableEdit edit : edits) {
            edit.applyEdit();
        }
    }
    
}