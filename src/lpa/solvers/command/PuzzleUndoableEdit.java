/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.command;

import javax.swing.undo.UndoableEdit;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.GridElement;

/**
 * Represents simple edit.
 */
public class PuzzleUndoableEdit extends PuzzleAbstractUndoableEdit{
    
    /**
     * The edge to edit.
     */
    private Edge edge;
    
    /**
     * Old state of the edge.
     */
    private EdgeState oldState;
    
    /**
     * New state of the edge.
     */
    private EdgeState newState;
    
    /**
     * Constructor.
     * @param edge the edge to edit
     * @param oldState old state of the edge
     * @param newState new state of the edge
     */
    public PuzzleUndoableEdit(Edge edge, EdgeState oldState, EdgeState newState) {
        super();
        this.edge = edge;
        this.oldState = oldState;
        this.newState = newState;
    }
    
    /**
     * Returns edge which is edited by the edit.
     * @return edited edge
     */
    public Edge getEdge() {
        return edge;
    }
    
    /**
     * Returns old state of the edge.
     * @return old state
     */
    public EdgeState getOldState() {
        return oldState;
    }
    
    /**
     * Returns new state of the edge.
     * @return new state
     */
    public EdgeState getNewState() {
        return newState;
    }
    
    /**
     * Applies the edit.
     */
    @Override
    public void applyEdit() {
        edge.setState(newState);
        for (GridElement element : edge.getSurroundings()) {
            element.getSurroundings().updateHistogram();
        }
    }
    
    /**
     * Determines whether the edit can be undone or not.
     * @return whether the edit can be undone or not
     */
    @Override
    public boolean canUndo() {
        return super.canUndo() && edge.getState() == newState;
    }
    
    /**
     * Undo.
     */
    @Override
    public void undo() {
        super.undo();
        edge.setState(oldState);
        for (GridElement element : edge.getSurroundings()) {
            element.getSurroundings().updateHistogram();
        }
    }
    
    /**
     * Determines whether the edit can be redone or not.
     * @return whether the edit can be redone or not
     */
    @Override
    public boolean canRedo() {
        return super.canRedo() && edge.getState() == oldState;
    }
    
    /**
     * Redo.
     */
    @Override
    public void redo() {
        super.redo();
        applyEdit();
    }
    
    /**
     * Tries to add given edit after the current.
     * @param anEdit edit to add
     * @return whether given edit added or not
     */
    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        if (!(anEdit instanceof PuzzleUndoableEdit)) {
            return false;
        }
        if (edge.getI() == ((PuzzleUndoableEdit)anEdit).getEdge().getI() &&  
                edge.getJ() == ((PuzzleUndoableEdit)anEdit).getEdge().getJ() &&
                newState == ((PuzzleUndoableEdit)anEdit).oldState) {
            newState = ((PuzzleUndoableEdit)anEdit).newState;
            return true;
        }
        return false;
    }
    
    /**
     * Determines whether the edit is significant or not.
     * @return whether the edit is significant or not
     */
    @Override
    public boolean isSignificant() {
        return oldState != newState;
    }
    
}
