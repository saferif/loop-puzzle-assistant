/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.command;

import javax.swing.undo.AbstractUndoableEdit;

/**
 * Abstract edit.
 */
public abstract class PuzzleAbstractUndoableEdit extends AbstractUndoableEdit {
    
    /**
     * Applies the edit.
     */
    public abstract void applyEdit();
}
