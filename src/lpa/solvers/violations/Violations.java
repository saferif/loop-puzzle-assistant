/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.violations;

/**
 * Class which is used to store information about violations to check.
 */
public class Violations {
    
    /**
     * Whether to check violation that cell has too many present edges around it.
     */
    private boolean yesEdges = true;
    
    /**
     * Whether to check violation that cell has too many absent edges around it.
     */
    private boolean noEdges = true;
    
    /**
     * Whether to check violation that grid has branching paths.
     */
    private boolean branchingPath = true;
    
    /**
     * Whether to check violation that grid has dead ends.
     */
    private boolean deadEnd = true;
    
    /**
     * Whether to check violation that grid has extra edges outside of the loop.
     */
    private boolean extraEdges = true;
    
    /**
     * Gets yesEdges property value.
     * @return property value
     */
    public boolean getYesEdgesState() {
        return yesEdges;
    }
    
    /**
     * Sets yesEdges property value.
     * @param state new value 
     */
    public void setYesEdgesState(boolean state) {
        yesEdges = state;
    }
    
    /**
     * Gets noEdges property value.
     * @return property value
     */
    public boolean getNoEdgesState() {
        return noEdges;
    }
    
    /**
     * Sets noEdges property value.
     * @param state new value 
     */
    public void setNoEdgesState(boolean state) {
        noEdges = state;
    }
    
    /**
     * Gets branchingPath property value.
     * @return property value
     */
    public boolean getBranchingPathState() {
        return branchingPath;
    }
    
    /**
     * Sets branchingPath property value.
     * @param state new value 
     */
    public void setBranchingPathState(boolean state) {
        branchingPath = state;
    }
    
    /**
     * Gets deadEnd property value.
     * @return property value
     */
    public boolean getDeadEndState() {
        return deadEnd;
    }
    
    /**
     * Sets deadEnd property value.
     * @param state new value 
     */
    public void setDeadEndState(boolean state) {
        deadEnd = state;
    }
    
    /**
     * Gets extraEdges property value.
     * @return property value
     */
    public boolean getExtraEdges() {
        return extraEdges;
    }
    
    /**
     * Sets noEdges property value.
     * @param state new value 
     */
    public void setExtraEdges(boolean state) {
        extraEdges = state;
    }
    
}
