/**
 * The Loop Puzzle Assistant violations.
 * 
 * Contains class which is used to store information about violations to check.
 */

package lpa.solvers.violations;