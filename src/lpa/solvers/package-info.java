/**
 * The Loop Puzzle Assistant helper classes.
 * 
 * Contains classes designed to help the user to solve the puzzle.
 */

package lpa.solvers;