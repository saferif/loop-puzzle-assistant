/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.autosolver;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the list of solutions of the puzzle.
 */
public class Solutions {
    
    /**
     * List of solutions.
     */
    private List<Solution> solutions;
    
    /**
     * Pointer to the current solution in the list.
     */
    private int pointer = -1;
    
    /**
     * Constructor.
     */
    public Solutions() {
        solutions = new ArrayList<>();
    }
    
    /**
     * Adds new solution to the list.
     * @param solution solution to add
     */
    public void add(Solution solution) {
        solutions.add(solution);
    }
    
    /**
     * Checks whether list contains next solution.
     * @return whether list contains next solution or not
     */
    public boolean hasNext() {
        return pointer < solutions.size() - 1;
    }
    
    /**
     * Checks whether list contains previous solution.
     * @return whether list contains previous solution or not
     */
    public boolean hasPrevious() {
        return pointer > 0;
    }
    
    /**
     * Returns next solution from the list.
     * @return next solution
     */
    public Solution next() {
        if (pointer >= solutions.size() - 1) {
            throw new IllegalArgumentException("In lpa.solvers.autosolver.Solutions.next: there is no next solution.");
        }
        return solutions.get(++ pointer);
    }
    
    /**
     * Returns previous solution from the list.
     * @return previous solution
     */
    public Solution previous() {
        if (pointer <= 0) {
            throw new IllegalArgumentException("In lpa.solvers.autosolver.Solutions.previous: there is no previous solution.");
        }
        return solutions.get(-- pointer);
    }
    
    /**
     * Returns number of solutions in the list.
     * @return number of solutions
     */
    public int size() {
        return solutions.size();
    }
}
