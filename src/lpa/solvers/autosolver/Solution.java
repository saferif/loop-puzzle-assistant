/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.autosolver;

import java.util.ArrayList;
import lpa.model.Edge;

/**
 * Represents a solution of the puzzle.
 */
public class Solution extends ArrayList<Edge>{
    
    /**
     * Constructor.
     */
    public Solution() {
        super();
    }
    
}
