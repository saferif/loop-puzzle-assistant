/**
 * The Loop Puzzle Assistant autosolver classes.
 * 
 * Contains classes related to puzzle auto solution feature.
 * <ul>
 * <li>Solution - represents one solution.</li>
 * <li>Solutions - represents the list of solutions.</li>
 * <li>Solver - main class which solves the puzzle.</li>
 * <li>SolvingListener - interface used as callback with EDT.</li>
 * </ul>
 */

package lpa.solvers.autosolver;