/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.autosolver;

import java.util.List;
import java.util.concurrent.CancellationException;
import javax.swing.SwingWorker;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.Grid;
import lpa.model.GridElement;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;

/**
 * Main class which solves the puzzle.
 */
public class Solver extends SwingWorker<Solutions, Double> {

    /**
     * Grid which contains information about the puzzle to solve/
     */
    private Grid grid;
    
    /**
     * Callback interface.
     */
    private SolvingListener listener;
    
    /**
     * List of found solutions.
     */
    private Solutions solutions;
    
    /**
     * Constructor.
     * @param grid grid
     * @param listener callback interface
     */
    public Solver(Grid grid, SolvingListener listener) {
        this.grid = grid;
        this.listener = listener;
        solutions = new Solutions();
    }
    
    /**
     * Background task.
     * @return list of found solutions
     * @throws Exception 
     */
    @Override
    protected Solutions doInBackground() throws Exception {
        solve(0, 0, 100.0);
        return solutions;
    }
    
    /**
     * Processes published chunks.
     * @param chunks list of chunks
     */
    @Override
    protected void process(List<Double> chunks) {
        for (Double chunk : chunks) {
            listener.progressChanged(chunk);
        }
    }
    
    /**
     * Called when the task is done.
     */
    @Override
    protected void done() {
        listener.done();
    }
    
    /**
     * Cancels the task.
     */
    public void cancel() {
        try {
            super.cancel(true);
        }
        catch (CancellationException e) { }
    }
    
    /**
     * Finds next undetermined edge.
     * @param i row number of the element to start from
     * @param j column number of the element to start from
     * @return found edge
     */
    private Edge findNextUndeterminedEdge(int i, int j) {
        Edge edge = null;
        //Pass the the next row if needed
        if (j >= grid.getWidth()) {
            j -= grid.getWidth();
            i ++;
        }
        while (i < grid.getHeight()) {
            GridElement element = grid.getElement(i, j);
            if (element instanceof Edge) {
                Edge _edge = (Edge)element;
                if (_edge.getState() == EdgeState.UNDETERMINED) {
                    edge = _edge;
                    break;
                }
            }
            
            j ++;
            //Pass the the next row if needed.
            if (j >= grid.getWidth()) {
                j -= grid.getWidth();
                i ++;
            }
        }
        return edge;
    }
    
    /**
     * Creates new solution object, assuming current grid is correctly solved.
     * @return solution
     */
    private Solution getSolution() {
        Solution solution = new Solution();
        for (GridElement element : grid) {
            if (element instanceof Edge) {
                Edge edge = (Edge)element;
                if (edge.getState() == EdgeState.PRESENT) {
                    solution.add(edge);
                }
            }
        }
        return solution;
    }
    
    /**
     * Solves the puzzle.
     * @param i row number of the element to start from
     * @param j column number of the element to start from
     * @param percentage percentage of the current solving recursive against whole solving process
     */
    private void solve(int i, int j, double percentage) {
        if (isCancelled()) {
            return;
        }
        Edge edge = findNextUndeterminedEdge(i, j);
        if (edge == null) {
            grid.updateViolatedElements();
            if (grid.isSolved()) {
                solutions.add(getSolution());
            }
            publish(percentage);
            return;
        }
        
        edge.setState(EdgeState.ABSENT);
        for (GridElement elem : edge.getSurroundings()) {
            elem.getSurroundings().updateHistogram();
        }
        PuzzleCompoundUndoableEdit edit = grid.applyStrategies();
        if (edit != null) {
            edit.applyEdit();
        }
        solve(edge.getI(), edge.getJ() + 1, percentage / 2.0);
        if (edit != null) {
            edit.undo();
        }
        
        edge.setState(EdgeState.PRESENT);
        for (GridElement elem : edge.getSurroundings()) {
            elem.getSurroundings().updateHistogram();
        }
        edit = grid.applyStrategies();
        if (edit != null) {
            edit.applyEdit();
        }
        solve(edge.getI(), edge.getJ() + 1, percentage / 2.0);
        if (edit != null) {
            edit.undo();
        }
        edge.setState(EdgeState.UNDETERMINED);
        for (GridElement elem : edge.getSurroundings()) {
            elem.getSurroundings().updateHistogram();
        }
    }
    
}
