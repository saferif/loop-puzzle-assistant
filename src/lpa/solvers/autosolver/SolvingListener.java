/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.autosolver;

/**
 * Interface used as callback with EDT.
 */
public interface SolvingListener {
    
    /**
     * Current amount of work done after previous method call.
     * @param percentage amount of work in percents
     */
    public void progressChanged(Double percentage);
    
    /**
     * All possible solutions are found.
     */
    public void done();
}
