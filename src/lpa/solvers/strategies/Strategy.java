/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.strategies;

import lpa.model.GridElement;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;

/**
 * Represents abstract strategy.
 */
public interface Strategy {
    
    /**
     * Applies strategy.
     * @param element element to apply strategy to.
     * @param gridWidth grid width.
     * @param gridHeight grid height.
     * @return edit made by strategy.
     */
    public PuzzleCompoundUndoableEdit apply(GridElement element, int gridWidth, int gridHeight);
}
