/**
 * The Loop Puzzle Assistant strategies.
 * 
 * Contains classes related to strategies feature.
 * <ul>
 * <li>Strategy - abstract strategy.</li>
 * <li>CornerOneStrategy - strategy to perform with corner cells containing 1.</li>
 * <li>CornerThreeStrategy - strategy to perform with corner cells containing 3.</li>
 * <li>DeadEndPreventionStrategy - strategy to perform to avoid dead ends.</li>
 * <li>FullCellStrategy - strategy to perform with cells that has exactly needed count of present edges.</li>
 * <li>FullVertexStrategy - strategy to perform with vertices that has 2 connected edges.</li>
 * <li>LackingCellStrategy - strategy to perform with cell that may be satisfied automatically.</li>
 * </ul>
 */

package lpa.solvers.strategies;