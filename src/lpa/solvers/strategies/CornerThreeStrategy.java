/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.strategies;

import lpa.model.Cell;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.GridElement;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;
import lpa.solvers.command.PuzzleUndoableEdit;

/**
 * Represents the strategy to perform with corner cells containing 3.
 */
public class CornerThreeStrategy implements Strategy {

    /**
     * Applies strategy.
     * @param element element to apply strategy to.
     * @param gridWidth grid width.
     * @param gridHeight grid height.
     * @return edit made by strategy.
     */
    @Override
    public PuzzleCompoundUndoableEdit apply(GridElement element, int gridWidth, int gridHeight) {
        //If element is not cell - return
        if (!(element instanceof Cell)) {
            return null;
        }
        
        //If cell does not contain any number - return
        Cell cell = (Cell) element;
        if (cell.getDigit() == null) {
            return null;
        }
        if (cell.getDigit() == 3) {
            if (cell.getI() == 1 && cell.getJ() == 1) {
                //Top left corner
                PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
                GridElement e1 = cell.getSurroundings().getElement(0);
                GridElement e2 = cell.getSurroundings().getElement(1);
                if ((!(e1 instanceof Edge)) || (!(e2 instanceof Edge))) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.CornerOneStrategy.apply: surroundings of cell must be edges.");
                }
                Edge edge1 = (Edge)e1;
                Edge edge2 = (Edge)e2;
                if (edge1.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge1, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                if (edge2.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge2, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                return edit.isSignificant() ? edit : null;
            }
            if (cell.getI() == gridHeight - 2 && cell.getJ() == 1) {
                //Bottom left corner
                PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
                GridElement e1 = cell.getSurroundings().getElement(1);
                GridElement e2 = cell.getSurroundings().getElement(2);
                if ((!(e1 instanceof Edge)) || (!(e2 instanceof Edge))) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.CornerOneStrategy.apply: surroundings of cell must be edges.");
                }
                Edge edge1 = (Edge)e1;
                Edge edge2 = (Edge)e2;
                if (edge1.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge1, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                if (edge2.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge2, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                return edit.isSignificant() ? edit : null;
            }
            if (cell.getI() == 1 && cell.getJ() == gridWidth - 2) {
                //Top right corner
                PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
                GridElement e1 = cell.getSurroundings().getElement(0);
                GridElement e2 = cell.getSurroundings().getElement(3);
                if ((!(e1 instanceof Edge)) || (!(e2 instanceof Edge))) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.CornerOneStrategy.apply: surroundings of cell must be edges.");
                }
                Edge edge1 = (Edge)e1;
                Edge edge2 = (Edge)e2;
                if (edge1.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge1, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                if (edge2.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge2, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                return edit.isSignificant() ? edit : null;
            }
            if (cell.getI() == gridHeight - 2 && cell.getJ() == gridWidth - 2) {
                //Bottom right corner
                PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
                GridElement e1 = cell.getSurroundings().getElement(2);
                GridElement e2 = cell.getSurroundings().getElement(3);
                if ((!(e1 instanceof Edge)) || (!(e2 instanceof Edge))) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.CornerOneStrategy.apply: surroundings of cell must be edges.");
                }
                Edge edge1 = (Edge)e1;
                Edge edge2 = (Edge)e2;
                if (edge1.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge1, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                if (edge2.getState() == EdgeState.UNDETERMINED) {
                    edit.add(new PuzzleUndoableEdit(edge2, EdgeState.UNDETERMINED, EdgeState.PRESENT));
                }
                return edit.isSignificant() ? edit : null;
            }
        }
        return null;
    }
    
}
