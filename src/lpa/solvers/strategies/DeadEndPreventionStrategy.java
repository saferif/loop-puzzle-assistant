/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.strategies;

import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.GridElement;
import lpa.model.Histogram;
import lpa.model.Vertex;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;
import lpa.solvers.command.PuzzleUndoableEdit;

/**
 * Represents the strategy to perform to avoid dead ends.
 */
public class DeadEndPreventionStrategy implements Strategy {

    /**
     * Applies strategy.
     * @param element element to apply strategy to.
     * @param gridWidth grid width.
     * @param gridHeight grid height.
     * @return edit made by strategy.
     */
    @Override
    public PuzzleCompoundUndoableEdit apply(GridElement element, int gridWidth, int gridHeight) {
        //If element is not vertex - return
        if (!(element instanceof Vertex)) {
            return null;
        }
        Vertex vertex = (Vertex)element;
        Histogram hist = vertex.getSurroundings().getHistogram();
        //If all surroundings of vertex except one are absent - apply.
        if (hist.getCount() - hist.getAbsent() == 1) {
            PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
            for (GridElement elem : vertex.getSurroundings()) {
                if (!(elem instanceof Edge)) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.DeadEndPreventionStrategy.apply: surroundings of vertex must be edges.");
                }
                Edge edge = (Edge)elem;
                if (edge.getState() != EdgeState.UNDETERMINED) {
                    continue;
                }
                else {
                    edit.add(new PuzzleUndoableEdit(edge, edge.getState(), EdgeState.ABSENT));
                }
            }
            return edit.isSignificant() ? edit : null;
        }
        return null;
    }
    
}
