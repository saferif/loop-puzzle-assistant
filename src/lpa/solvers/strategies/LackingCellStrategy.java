/**
 * @author Latypov Insaf, 271PI
 */

package lpa.solvers.strategies;

import lpa.model.Cell;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.GridElement;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;
import lpa.solvers.command.PuzzleUndoableEdit;

/**
 * Represents the strategy to perform with cell that may be satisfied automatically.
 */
public class LackingCellStrategy implements Strategy {

    /**
     * Applies strategy.
     * @param element element to apply strategy to.
     * @param gridWidth grid width.
     * @param gridHeight grid height.
     * @return edit made by strategy.
     */
    @Override
    public PuzzleCompoundUndoableEdit apply(GridElement element, int gridWidth, int gridHeight) {
        if (!(element instanceof Cell)) {
            return null;
        }
        Cell cell = (Cell)element;
        if (cell.getDigit() == null) {
            return null;
        }
        int digit = cell.getDigit();
        if (cell.getSurroundings().getHistogram().getAbsent() == 4 - digit) {
            PuzzleCompoundUndoableEdit edit = new PuzzleCompoundUndoableEdit();
            for (GridElement elem : cell.getSurroundings()) {
                if (!(elem instanceof Edge)) {
                    throw new IllegalArgumentException("In lpa.solvers.strategies.LackingCellStrategy.apply: surroundings of cell must be edges.");
                }
                Edge edge = (Edge)elem;
                if (edge.getState() != EdgeState.UNDETERMINED) {
                    continue;
                }
                else {
                    edit.add(new PuzzleUndoableEdit(edge, edge.getState(), EdgeState.PRESENT));
                }
            }
            return edit.isSignificant() ? edit : null;
        }
        return null;
    }
    
}
