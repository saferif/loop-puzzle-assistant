/**
* The Loop Puzzle Assistant view.
*
* Contains classes to represent view of the puzzle.
* <ul>
* <li>MainFrame - main class, main window.</li>
* <li>PuzzlePanel - panel which paints puzzle grid.</li>
* </ul>
*/
package lpa.gui;