/**
 * @author Latypov Insaf, 271PI
 */

package lpa.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
import lpa.model.*;
import lpa.solvers.autosolver.Solution;
import lpa.solvers.command.PuzzleCompoundUndoableEdit;
import lpa.solvers.command.PuzzleUndoableEdit;

/**
 * Panel which paints puzzle grid.
 */
public class PuzzlePanel extends javax.swing.JPanel {

    /**
     * Creates new form puzzlePanel
     */
    public PuzzlePanel() {
        initComponents();
    }
    
    /**
     * Minimal padding of the panel.
     */
    private final static int PANEL_PADDING = 5;
    
    /**
     * Edge's width to height ratio.
     */
    private final static int EDGE_RATIO = 5;
    
    /**
     * Edge width.
     */
    private int edgeWidth = 50;
    
    /**
     * Edge height.
     */
    private int edgeHeight = 10;
    
    /**
     * Counted panel x axis padding.
     */
    private int panelPaddingX = 5;
    
    /**
     * Counted panel y axis padding.
     */
    private int panelPaddingY = 5;
    
    /**
     * Are strategies appliance allowed.
     */
    private boolean strategiesAllowed = true;
    
    /**
     * Is panel in the edit mode.
     */
    private boolean editMode = false;
    
    /**
     * Is puzzle shown is solved.
     */
    private boolean solved = false;
    
    /**
     * Is puzzle edition allowed.
     */
    private boolean allowEdit = true;
    
    /**
     * Is panel showing a solution of the puzzle.
     */
    private boolean showingSolution = false;
    
    /**
     * Grid.
     */
    private Grid puzzle;
    
    /**
     * Saved state of the grid.
     */
    private Grid oldPuzzle;
    
    /**
     * Undo manager.
     */
    private UndoManager undoManager = new UndoManager();
    
    /**
     * Applies all strategies if allowed.
     * @return edit made by strategies.
     */
    private PuzzleCompoundUndoableEdit applyStrategies() {
        if (puzzle == null) {
            return null;
        }
        if (strategiesAllowed) {
            return puzzle.applyStrategies();
        }
        return null;
    }
    
    /**
     * Sets edit mode.
     * @param mode new edit mode state
     */
    public void setEditMode(boolean mode) {
        editMode = mode;
    }
    
    /**
     * Permits or denies puzzle edition.
     * @param state new edit allowed state
     */
    public void setAllowEdit(boolean state) {
        allowEdit = state;
    }
    
    /**
     * Allows or denies strategy appliance.
     * @param state new strategies allowed state
     */
    public void setStrategies(boolean state) {
        strategiesAllowed = state;
    }
    
    /**
     * Updates puzzle's solved state.
     */
    public void updateSolved() {
        if (puzzle == null) {
            return;
        }
        solved = puzzle.isSolved();
    }
    
    /**
     * Counts size of the edge.
     */
    public void countSize() {
        if (puzzle == null) {
            return;
        }
        
        int width = getWidth();
        int height = getHeight();
        
        //Width of the grid in units
        int unitWidth = (puzzle.getWidth() / 2) * (EDGE_RATIO + 1) + 1; 
        //Height of the grid in units
        int unitHeight = (puzzle.getHeight() / 2) * (EDGE_RATIO + 1) + 1;
        //Size of the unit to make grid fit by y axis
        int unitSizeFitY = (height - PANEL_PADDING * 2) / unitHeight;
        //Size of the unit to make grid fit by x axis
        int unitSizeFitX = (width - PANEL_PADDING * 2) / unitWidth;
        int unitSize;
        if (unitSizeFitY < unitSizeFitX) {
            unitSize = unitSizeFitY;
            panelPaddingX = (width - unitWidth * unitSize) / 2;
            panelPaddingY = PANEL_PADDING;
        } 
        else {
            unitSize = unitSizeFitX;
            panelPaddingX = PANEL_PADDING;
            panelPaddingY = (height - unitHeight * unitSize) / 2;
        }
        edgeHeight = unitSize;
        edgeWidth = unitSize * EDGE_RATIO;
    }
    
    /**
     * Sets puzzle to show.
     * @param puzzle puzzle 
     */
    public void setPuzzle(Grid puzzle) {
        this.puzzle = puzzle;
        countSize();
        applyStrategies();
        updateViolatedElements();
        updateSolved();
        repaint();
        showingSolution = false;
        oldPuzzle = null;
    }
    
    /**
     * Gets puzzle shown by panel.
     * @return puzzle
     */
    public Grid getPuzzle() {
        return puzzle;
    }
    
    /**
     * Paints panel
     * @param g graphics object
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawGrid(g);
    }
    
    /**
     * Draws the grid
     * @param g graphics object
     */
    private void drawGrid(Graphics g) {
        if (puzzle == null) {
            return;
        }
        for (GridElement element : puzzle) {
            if (element instanceof Vertex) {
                //Element is vertex
                drawVertex(g, (Vertex)element);
            }
            else {
                if (element instanceof Edge) {
                    //Element is edge
                    drawEdge(g, (Edge)element);
                }
                else {
                    //Element is cell
                    drawCell(g, (Cell)element);
                }
            }
        }
    }
    
    /**
     * Draws vertex.
     * @param g graphics object
     * @param vertex vertex to draw
     */
    private void drawVertex(Graphics g, Vertex vertex) {
        if (vertex.getSurroundings().getHistogram().getPresent() < 2) {
            g.setColor(Color.GRAY);
        }
        else {
            g.setColor(Color.BLACK);
        }
        if (vertex.getViolated()) {
            g.setColor(Color.RED);
        }
        if (solved) {
            g.setColor(Color.GREEN);
        }
        int x = (vertex.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX;
        int y = (vertex.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY;
        g.fillOval(x, y, edgeHeight, edgeHeight);
    }
    
    /**
     * Draws edge.
     * @param g graphics object
     * @param edge edge to draw
     */
    private void drawEdge(Graphics g, Edge edge) {
        if (edge.getState() == EdgeState.ABSENT) {
            int x, y;
            if ((edge.getI() & 1) == 0) {
                //Horizonatal edge
                x = (edge.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX + edgeHeight + (edgeWidth / 2 - edgeHeight / 2);
                y = (edge.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY;
            }
            else {
                //Vertical edge
                x = (edge.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX;
                y = (edge.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY + edgeHeight + (edgeWidth / 2 - edgeHeight / 2);
            }
            Graphics2D g2 = (Graphics2D)g;
            g2.setStroke(new BasicStroke(edgeHeight / 4.5f));
            g.setColor(Color.BLACK);
            g.drawLine(x, y, x + edgeHeight, y + edgeHeight);
            g.drawLine(x + edgeHeight, y, x, y + edgeHeight);
            return;
        }
        if (edge.getState() == EdgeState.PRESENT) {
            g.setColor(Color.BLACK);
        }
        if (solved) {
            g.setColor(Color.GREEN);
        }
        if (edge.getState() == EdgeState.UNDETERMINED) {
            g.setColor(Color.GRAY);
        }
        if (puzzle.getViolations().getExtraEdges() && edge.getViolated()) {
            g.setColor(Color.RED);
        }
        if ((edge.getI() & 1) == 0) {
            //Horizontal edge
            int x = (edge.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX + edgeHeight;
            int y = (edge.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY;
            g.fillRect(x, y, edgeWidth, edgeHeight);
        }
        else {
            //Vertical edge
            int x = (edge.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX;
            int y = (edge.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY + edgeHeight;
            g.fillRect(x, y, edgeHeight, edgeWidth);
        }
    }
    
    /**
     * Draws cell.
     * @param g graphics object
     * @param cell cell to draw
     */
    private void drawCell(Graphics g, Cell cell) {
        if (cell.getDigit() == null) {
            return;
        }
        char digit = cell.getDigit().toString().charAt(0);
        g.setFont(new Font(g.getFont().getName(), Font.PLAIN, edgeWidth / 3));
        int x = (cell.getJ() / 2) * (edgeWidth + edgeHeight) + panelPaddingX + edgeHeight + (edgeWidth / 2) - (g.getFontMetrics().charWidth(digit) / 2);
        int y = (cell.getI() / 2) * (edgeWidth + edgeHeight) + panelPaddingY + edgeHeight + (edgeWidth / 2) + (g.getFontMetrics().getAscent() / 2);
        g.setColor(Color.BLACK);
        if (cell.getViolated()) {
            g.setColor(Color.RED);
        }
        g.drawString(digit + "", x, y);
    }
    
    /**
     * Gets I or J of the element by X or Y correspondingly (used to find clicked element).
     * @param x X or Y of the mouse click
     * @param padding padding of the grid
     * @return I or J
     */
    private int getMetric(int x, int padding) {
        x -= padding;
        int i = 0;
        while (true) {
            if ((i & 1) == 0) {
                if (x < edgeHeight) {
                    break;
                }
                x -= edgeHeight;
                i ++;
            }
            else {
                if (x < edgeWidth) {
                    break;
                }
                x -= edgeWidth;
                i ++;
            }
        }
        return i;
    }
    
    /**
     * Returns undo manager.
     * @return undo manager
     */
    public UndoManager getUndoManager() {
        return undoManager;
    }
    
    /**
     * Determines whether panel is showing solution at the moment or not.
     * @return whether panel is showing solution at the moment or not.
     */
    public boolean getShowingSolution() {
        return showingSolution;
    }
    
    /**
     * Restores saved grid.
     */
    public void restoreOldGrid() {
        if (!showingSolution) {
            throw new IllegalStateException("Cannot restore while not showing solutions.");
        }
        showingSolution = false;
        puzzle = oldPuzzle;
        oldPuzzle = null;
        
        ((MainFrame)getTopLevelAncestor()).updateUndoRedoMenuItems();
        
        repaint();
    }
    
    /**
     * Updates violated states of grid elements.
     */
    public void updateViolatedElements() {
        if (puzzle == null) {
            return;
        }
        puzzle.updateViolatedElements();
    }
    
    /**
     * Shows given solution on the panel.
     * @param solution solution to show
     */
    public void showSolution(Solution solution) {
        if (!showingSolution) {
            showingSolution = true;
            oldPuzzle = puzzle;
            puzzle = oldPuzzle.clone();
        }
        for (GridElement element : puzzle) {
            if (element instanceof Edge) {
                ((Edge)element).setState(EdgeState.ABSENT);
            }
        }
        for (Edge edge : solution) {
            ((Edge)puzzle.getElement(edge.getI(), edge.getJ())).setState(EdgeState.PRESENT);
        }
        updateSolved();
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                puzzlePanelClicked(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                puzzlePanelResized(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void puzzlePanelClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_puzzlePanelClicked
        //Edit is not allowed ot nothing to edit
        if ((!allowEdit) || puzzle == null) {
            return;
        }
        
        //Get I and J by mouse click coordinates.
        int i = getMetric(evt.getY(), panelPaddingY);
        int j = getMetric(evt.getX(), panelPaddingX);
        
        //Click has been performed outside of the grid
        if (i >= puzzle.getHeight()) {
            return;
        }
        if (j >= puzzle.getWidth()) {
            return;
        }
        //Get clicked element
        GridElement element = puzzle.getElement(i, j);
        
        //Get main frame
        MainFrame frame = (MainFrame)getTopLevelAncestor();
        frame.resetSolutions();
        if (showingSolution) {
            showingSolution = false;
            undoManager.discardAllEdits();
            frame.updateUndoRedoMenuItems();
            oldPuzzle = null;
        }
        
        //Cell is clicked and edit mode id on
        if (element instanceof Cell && editMode) {
            Cell cell = (Cell)element;
            if (cell.getDigit() != null) {
                cell.setDigit(cell.getDigit() + 1);
            }
            else {
                cell.setDigit(0);
            }
            if (cell.getDigit() == 4) {
                cell.setDigit(null);
            }
            updateViolatedElements();
            repaint();
            return;
        }
        
        //Cannot proccess click of anything but edge
        if (!(element instanceof Edge)) {
            return;
        }
        
        //Count edge's new state
        Edge edge = (Edge)element;
        EdgeState newState, oldState = edge.getState();
        if (oldState == EdgeState.UNDETERMINED) {
            newState = EdgeState.PRESENT;
        }
        else {
            if (oldState == EdgeState.PRESENT) {
                newState = EdgeState.ABSENT;
            }
            else {
                newState = EdgeState.UNDETERMINED;
            }
        }
        
        //Set edge's new state and update its histogram
        edge.setState(newState);
        for (GridElement elem : edge.getSurroundings()) {
            elem.getSurroundings().updateHistogram();
        }
        
        //Apply strategies.
        UndoableEdit edit;
        PuzzleCompoundUndoableEdit strategiesEdit = null;
        if (newState != EdgeState.UNDETERMINED) {
                strategiesEdit = applyStrategies();
        }
        if (strategiesEdit != null) {
            edit = new PuzzleCompoundUndoableEdit();
            ((PuzzleCompoundUndoableEdit)edit).add(new PuzzleUndoableEdit(edge, oldState, newState));
            ((PuzzleCompoundUndoableEdit)edit).add(strategiesEdit);
            
        }
        else {
            edit = new PuzzleUndoableEdit(edge, oldState, newState);
        }
        
        //Save the edit to perform undo and redo after
        undoManager.addEdit(edit);
        frame.setUndoMenuItemEnabled(undoManager.canUndo());
        frame.setRedoMenuItemEnabled(undoManager.canRedo());
        
        //Update edge's histogram after possible strategies appliance.
        for (GridElement elem : edge.getSurroundings()) {
            elem.getSurroundings().updateHistogram();
        }
        
        updateViolatedElements();
        updateSolved();
        
        repaint();
    }//GEN-LAST:event_puzzlePanelClicked

    private void puzzlePanelResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_puzzlePanelResized
        countSize();
    }//GEN-LAST:event_puzzlePanelResized

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
